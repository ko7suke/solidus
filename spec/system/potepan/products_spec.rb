require 'rails_helper'

RSpec.describe "Potepan::Products", type: :system do
  let(:base_title) { 'BIGBAG Store' }
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy) }
  let(:taxon_2) { create(:taxon, name: "Mags", taxonomy: taxonomy) }
  let(:product) { create(:product, name: 'shrit', description: 'GOOD SHRIT', taxons: [taxon]) }
  let!(:product_2) { create(:product, name: 'T-shrit', taxons: [taxon]) }
  let!(:product_3) { create(:product, name: 'Y-shrit', taxons: [taxon]) }
  let!(:product_4) { create(:product, name: 'BIG-shrit', taxons: [taxon]) }
  let!(:product_5) { create(:product, name: 'SMALL-shrit', taxons: [taxon]) }
  let(:product_6) { create(:product, name: 'MEDIUM-shrit', taxons: [taxon_2]) }

  before do
    visit potepan_product_path(product.id)
  end

  it 'show the title of product page correctly' do
    expect(page).to have_title("#{product.name} - #{base_title}")
  end

  it 'show the product correctly' do
    within ".media-body" do
      expect(page).to have_content(product.name)
      expect(page).to have_content(product.display_price)
      expect(page).to have_content(product.description)
      expect(page).to have_content("一覧ページへ戻る")
    end
  end

  it 'show related_products name' do
    within ".productsContent" do
      expect(page).to have_content(product_2.name)
      expect(page).to have_content(product_3.name)
      expect(page).to have_content(product_4.name)
      expect(page).to have_content(product_5.name)
    end
  end

  it 'show related_products price' do
    within ".productsContent" do
      expect(page).to have_content(product_2.display_price)
      expect(page).to have_content(product_3.display_price)
      expect(page).to have_content(product_4.display_price)
      expect(page).to have_content(product_5.display_price)
    end
  end

  it 'moves to a related_product page through the link' do
    within ".productsContent" do
      expect(page).to have_content(product_2.name)
      click_link(product_2.name)
    end

    expect(page).to have_title("#{product_2.name} - #{base_title}")
    within ".media-body" do
      expect(page).to have_content(product_2.name)
      expect(page).to have_content(product_2.display_price)
    end

    within ".productsContent" do
      expect(page).not_to have_content(product_2.name)
    end
  end

  it 'show the product_6 pages without unrelated_products' do
    visit potepan_product_path(product_6.id)
    within ".productsContent" do
      expect(page).not_to have_content(product_2.name)
      expect(page).not_to have_content(product_2.display_price)
    end
  end
end
