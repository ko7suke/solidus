require 'rails_helper'

RSpec.describe 'Potepan::categories', type: :system do
  let(:base_title) { 'BIGBAG Store' }
  let(:taxonomy_1) { create(:taxonomy, name: "Categories") }
  let!(:taxonomy_2) { create(:taxonomy, name: "Brand") }
  let(:taxon_1) { create(:taxon, name: "Bags", taxonomy: taxonomy_1) }
  let(:taxon_2) { create(:taxon, name: "Mags", taxonomy: taxonomy_1) }
  let!(:product_1) { create(:product, name: "RUBY ON RAILS TOTE", taxons: [taxon_1]) }
  let!(:product_2) { create(:product, name: "RUBY ON RAILS MUG", taxons: [taxon_2]) }

  before do
    visit potepan_category_path(taxon_1.id)
  end

  scenario 'show the title of taxon_1 correctly' do
    expect(page).to have_title("#{taxon_1.name} - #{base_title}")
  end

  scenario 'show the taxon_1 only' do
    within ".productBox" do
      expect(page).to have_content(product_1.name)
      expect(page).to have_content(product_1.display_price)
      expect(page).not_to have_content(product_2.name)
    end
  end

  scenario 'show taxonomy at side bar' do
    within ".side-nav" do
      expect(page).to have_content(taxonomy_1.name)
      expect(page).to have_content(taxon_1.name)
      expect(page).to have_content(taxon_1.products.count)
      expect(page).to have_content(taxonomy_2.name)
      expect(page).to have_content(taxon_2.name)
      expect(page).to have_content(taxon_2.products.count)
    end
  end

  scenario 'show the page of taxon_2 correctly' do
    click_link "#{taxon_2.name} (#{taxon_2.products.count})"
    expect(page).to have_title("#{taxon_2.name} - #{base_title}")

    within ".productBox" do
      expect(page).to have_content(product_2.name)
      expect(page).to have_content(product_2.display_price)
      expect(page).not_to have_content(product_1.name)
    end
  end

  scenario 'return potepan_category_path(taxon_1.id)' do
    within ".productBox" do
      expect(page).to have_content(product_1.name)
      expect(page).to have_content(product_1.display_price)
      click_link(product_1.name)
    end
    expect(page).to have_title("#{product_1.name} - #{base_title}")
    click_link("一覧ページへ戻る")

    within ".productBox" do
      expect(page).to have_content(product_1.name)
      expect(page).to have_content(product_1.display_price)
      expect(page).not_to have_content(product_2.name)
    end
  end
end
