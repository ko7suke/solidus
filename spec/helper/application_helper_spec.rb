require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full title helper' do
    let(:base_title) { 'BIGBAG Store' }

    context 'case page title is default' do
      let(:default_title) { full_title('') }

      it 'show title as BIGBAG Store' do
        expect(default_title).to eq "#{base_title}"
      end
    end

    context 'case page title present' do
      let(:title) { full_title('sample') }

      it 'show title as sample - BIGBAG Store' do
        expect(title).to eq "sample - #{base_title}"
      end
    end
  end
end
