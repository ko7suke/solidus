require 'rails_helper'

RSpec.describe "Potepan::categories", type: :request do
  describe "GET /potepan/categories/:id" do
    let(:base_title) { 'BIGBAG Store' }
    let(:taxonomy_1) { create(:taxonomy, name: "Categories") }
    let(:taxon_1) { create(:taxon, name: "Bags", taxonomy: taxonomy_1) }
    let(:taxon_2) { create(:taxon, name: "Mugs", taxonomy: taxonomy_1) }
    let!(:product_1) { create(:product, name: "RUBY ON RAILS TOTE", taxons: [taxon_1]) }

    before do
      get potepan_category_path(taxon.id)
    end

    context "when potepan_category_path(taxon_1.id) " do
      let(:taxon) { taxon_1 }

      it "success for http request" do
        expect(response.status).to eq 200
      end

      it "react templete normally for show" do
        expect(response).to render_template :show
      end

      it "show taxonomy_1 correctly" do
        expect(response.body).to include(taxonomy_1.name)
      end

      it "show taxon_1 correctly" do
        expect(response.body).to include(taxon_1.name)
      end

      it "show product_1 correctly " do
        expect(response.body).to include(product_1.name)
      end

      it "show full title correctly" do
        expect(response.body).to match(/<title>#{taxon_1.name} - #{base_title}<\/title>/i)
      end
    end

    context "when potepan_category_path(taxon_2.id) " do
      let(:taxon) { taxon_2 }

      it "does't show product_1 if taxons is taxon_2" do
        expect(response.body).not_to include(product_1.name)
      end
    end
  end
end
