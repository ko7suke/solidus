require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET /potepan/products/:id" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, name: 'shrit', price: '12.34', description: 'GOOD SHRIT', taxons: [taxon]) }
    let(:base_title) { 'BIGBAG Store' }

    before do
      get potepan_product_path(product.id)
    end

    it "Show product name correctly" do
      expect(response.body).to include('shrit')
    end

    it "Show product display price correctly" do
      expect(response.body).to include('$12.34')
    end

    it "Show product description correctly " do
      expect(response.body).to include('GOOD SHRIT')
    end

    it "Respond normally" do
      expect(response).to be_successful
    end

    it "Show full title correctly" do
      assert_select "title", "shrit - #{base_title}"
    end

    it "have expected value about product " do
      expect(assigns(:product)).to eq product
    end

    it "React templete normally for show" do
      expect(response).to render_template :show
    end

    describe "max number of related product is assigned 4" do
      MAX_NUM_RELATED_PRODUCTS = 4
      let!(:related_product) { create(:product, taxons: [taxon], name: "related_product") }
      let!(:related_products) { create_list(:product, 5, taxons: [taxon], name: "related_products") }

      it "show up to 4 related_products" do
        expect(controller.instance_variable_get("@related_products").count).to be MAX_NUM_RELATED_PRODUCTS
      end
    end
  end
end
