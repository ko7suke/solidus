require 'rails_helper'

RSpec.describe "Potepan::Product", type: :model do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy) }
  let(:product) { create(:product, name: 'Bag', taxons: [taxon]) }

  describe "related_products" do
    let(:related_products) do
      product.related_products
    end

    context "when there is a related product" do
      let!(:related_products) { create_list(:product, 1, taxons: [taxon]) }

      it "collects a product" do
        expect(product.related_products.size).to eq 1
      end
    end

    context "when there are 2 related products" do
      let!(:related_products) { create_list(:product, 2, taxons: [taxon]) }

      it "collects 2 products" do
        expect(product.related_products.size).to eq 2
      end
    end
  end
end
