class Potepan::ProductsController < ApplicationController
  # spec/requests/potepan/product_spec.rbでも定数を設定してるため、定数を変更する場合はrspecの定数も変更すること
  MAX_NUM_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.
      includes(master: [:images, :default_price]).
      limit(MAX_NUM_RELATED_PRODUCTS)
  end
end
