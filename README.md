## 当アプリケーション概要  

当アプリはオープンソースSolidusを使用したECサイトです。  
[アプリはこちらから](https://kosuke-ec.herokuapp.com/potepan/index)  

## 使用した技術  
- 言語 : Ruby, javaScript, HTML5, CSS3  
- フレームワーク : Ruby on Rails  
- インフラ : Docker, Circle CI, AWS(S3), MySQL  
- デプロイ : Heroku  
- コード管理 : Github  
- その他: RSpec, Rubocopなど  

## 開発で行ったこと  
- 環境構築  
- 商品詳細ページの構築  
- カテゴリー別商品一覧の実装  
- 商品詳細ページの下部に関連商品が表示される機能実装  
- Rspecの実装  
[商品一覧ページはこちらから](https://kosuke-ec.herokuapp.com/potepan/categories/1)  
[商品詳細ページはこちらから](https://kosuke-ec.herokuapp.com/potepan/products/1)  
